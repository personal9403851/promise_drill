//List of promises function

const task1 = new Promise((resolve, reject) => {

    let a = 1 + 1
    if (a === 2) {
        resolve(`task1 completed successfully`)
    }
    else {
        reject(`Error occurred while executing task1`)
    }
})

const task2 = new Promise((resolve, reject) => {

    let a = 1 + 1
    if (a === 3) {
        resolve(`task2 completed successfully`)
    }
    else {
        reject(`Error occurred while executing task2`)
    }
})

const task3 = new Promise((resolve, reject) => {

    let a = 1 + 1
    if (a === 5) {
        resolve("task3 completed successfully")
    }
    else {
        reject(`Error occurred while executing task3`)
    }
})

//Handling promises function

let composePromises = Promise.allSettled([task1, task2, task3])

composePromises.then((resultArray) => {
    console.log(resultArray)
}).catch(() => {
    console.log(`Error occurred while executing the composePromises Function`)
})