const task1 = new Promise((resolve, reject) => {
    let a = 1 + 1;
    if (a === 2) {
        resolve(`task1 completed successfully`);
    } else {
        reject(`Error occurred while executing task1`);
    }
});

const task2 = new Promise((resolve, reject) => {
    let a = 1 + 1;
    if (a === 3) {
        resolve(`task2 completed successfully`);
    } else {
        reject(`Error occurred while executing task2`);
    }
});

const task3 = new Promise((resolve, reject) => {
    let a = 1 + 1;
    if (a === 5) {
        resolve("task3 completed successfully");
    } else {
        reject(`Error occurred while executing task3`);
    }
});

const functionsArray = [task1, task2, task3];
const limit = 2;

const parallelLimit = (functionsArray, limit) => {
    return new Promise(async (resolve, reject) => {
        try {
            let resolvedArray = [];

            for (let startIndex = 0; startIndex < functionsArray.length; startIndex += limit) {
                let limitArray = functionsArray.slice(startIndex, startIndex + limit);

                let composedPromises = await Promise.allSettled(limitArray);

                composedPromises.forEach(eachPromise => {
                    resolvedArray.push(eachPromise);
                });
            }

            resolve(resolvedArray);
        } catch (error) {
            reject(error);
        }
    });
};

parallelLimit(functionsArray, limit)
    .then((resolvedArray) => {
        console.log(resolvedArray);
    })
    .catch((error) => {
        console.log(error);
    });
