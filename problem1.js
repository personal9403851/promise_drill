const racePromise1 = new Promise((resolve) => {
    setTimeout(() => {
        resolve("The function named 'racePromise1' has been resolved fist after 1 second of delay")
    }, 1000)
})

const racePromise2 = new Promise((resolve) => {
    setTimeout(() => {
        resolve("The function named 'racePromise2' has been resolved first after 3 seconds of delay")
    }, 3000)
})

const racePromises = Promise.race([racePromise1, racePromise2])

racePromises.then((result) => {
    console.log(result)
})